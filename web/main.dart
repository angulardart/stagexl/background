import 'dart:async';
import 'dart:html' as html;
import 'package:stagexl/stagexl.dart';

Future main() async {

  // Configure StageXL default options

  StageOptions options = StageOptions()
    ..backgroundColor = Color.Black
    ..stageAlign = StageAlign.TOP
    ..renderEngine = RenderEngine.WebGL;
 int width = html.window.innerWidth;
 int height = html.window.innerHeight;

  double dspH = height / 25;
  double dspW = width / 25;

  // Init Stage and RenderLoop

  var canvas = html.querySelector('#stage');
  canvas.setAttribute("height", height.toString());
  canvas.setAttribute("width", width.toString());

  Stage stage = Stage(canvas,width: width, height: height, options: options);


  var resourceManager = ResourceManager();
  Map<String,Background> background = Map<String,Background>();
background ={
"Chicken": Background()..name = "Chicken"..location = "Chicken.svg",
"Fox": Background()..name = "Fox"..location = "Fox.svg",
"Hunters": Background()..name = "Hunters"..location = "Hunters.svg",
"JamesRock": Background()..name = "JamesRock"..location = "JamesRock.svg",
"Jay": Background()..name = "Jay"..location = "Jay.svg",
"Preacher": Background()..name = "Preacher"..location = "Preacher.svg",
"Thug": Background()..name = "Thug"..location = "Thug.svg",
"Desktop_landscape" : Background()..name = "Desktop_landscape"..location = "background/Desktop_landscape.svg",
"Desktop_Portrait" : Background()..name = "Desktop_Portrait"..location = "background/Desktop_Portrait.svg"
};

for (String name in backgroundChars) {
  background[name].name;
  resourceManager.addBitmapData(background[name].name, "/img/background/svg/" + background[name].location);
}
for (String name in backgrounds) {
  background[name].name;
  resourceManager.addBitmapData(background[name].name, "/img/background/svg/" + background[name].location);
}

  await resourceManager.load();

// for (int i = backgroundName.length -1; i > 0; i-- ) {
//   var bitmapData = resourceManager.getBitmapData(backgroundName[i]);
//   var bitmap = Bitmap(bitmapData);
//   bitmap.x = 0;
//   stage.addChild(bitmap);

// }


print("$width : $height");
Warp w = Warp();
w.height = height;
w.width = width;

if(width > height){
  BitmapData bitmapData = resourceManager.getBitmapData("Desktop_landscape");
  Bitmap bitmap = Bitmap(bitmapData);
  bitmap.height = height;
  bitmap.width = width;
  w.addChild(bitmap);
} 
if(width < height) {
  BitmapData bitmapData = resourceManager.getBitmapData("Desktop_Portrait");
  Bitmap bitmap = Bitmap(bitmapData);
  bitmap.height = height;
  bitmap.width = width;
  w.addChild(bitmap);

}
  stage.addChild(w);
//Desktop_Portrait placement
  if(width < height){
  for(String name in backgroundChars){
    BitmapData bitmapData = resourceManager.getBitmapData(name);
    Bitmap bitmap = Bitmap(bitmapData);

    if(name == "Chicken"){
      bitmap.height = dspH * 1;
      bitmap.width = dspW * 3;
      bitmap.y = dspH * 12.5;
      bitmap.x = dspW * 10;
    }
    if(name == "Fox"){
      bitmap.height = dspH * 1;
      bitmap.width = dspW * 2;
      bitmap.y = dspH * 12;
      bitmap.x = dspW * 20;
    }
    if(name == "Hunters"){
      bitmap.height = dspH * 3;
      bitmap.width = dspW * 4;
      bitmap.y = dspH * 13;
      bitmap.x = dspW * 5.5;
    }
    if(name == "Jay"){
      bitmap.height = dspH * 3;
      bitmap.width = dspW * 2;
      bitmap.y = dspH * 15;
      bitmap.x = dspW * 18;
    }
    if(name == "Preacher"){
      bitmap.height = dspH * 6;
      bitmap.width = dspW * 6;
      bitmap.y = dspH * 17;
      bitmap.x = dspW * 10;
    }
    if(name == "Thug"){
      bitmap.height = dspH * 4;
      bitmap.width = dspW * 2;
      bitmap.y = dspH * 19;
      bitmap.x = dspW * 1;
    }
    if(name == "JamesRock"){
      bitmap.height = dspH * 2;
      bitmap.width = dspW * 1;
      bitmap.y = dspH * 13;
      bitmap.x = dspW * 10;
    }

    stage.addChild(bitmap);

  }

  }
// Desktop_landscape placement
  if(width > height){
  for(String name in backgroundChars){
    BitmapData bitmapData = resourceManager.getBitmapData(name);
    Bitmap bitmap = Bitmap(bitmapData);

    if(name == "Chicken"){
      bitmap.height = dspH * 1;
      bitmap.width = dspW * 2;
      bitmap.y = dspH * 17;
      bitmap.x = dspW * 14;
    }
    if(name == "Fox"){
      bitmap.height = dspH * 1;
      bitmap.width = dspW * 1;
      bitmap.y = dspH * 13;
      bitmap.x = dspW * 22;
    }
    if(name == "Hunters"){
      bitmap.height = dspH * 6;
      bitmap.width = dspW * 3;
      bitmap.y = dspH * 13;
      bitmap.x = dspW * 5.5;
    }
    if(name == "Jay"){
      bitmap.height = dspH * 9;
      bitmap.width = dspW * 1.5;
      bitmap.y = dspH * 14;
      bitmap.x = dspW * 20;
    }
    if(name == "Preacher"){
      bitmap.height = dspH * 11;
      bitmap.width = dspW * 4;
      bitmap.y = dspH * 13.5;
      bitmap.x = dspW * 10;
    }
    if(name == "Thug"){
      bitmap.height = dspH * 5;
      bitmap.width = dspW * 1;
      bitmap.y = dspH * 15;
      bitmap.x = dspW * 1;
    }
    if(name == "JamesRock"){
      bitmap.height = dspH * 4;
      bitmap.width = dspW * 1;
      bitmap.y = dspH * 13.5;
      bitmap.x = dspW * 9;
    }

    stage.addChild(bitmap);

  }

  }


  var renderLoop = RenderLoop();
  renderLoop.addStage(stage);


}

class Background{
  String _name;
  String _location;
  Warp _image;

  Background();
  //name Filename
  String get name => this._name;
  set name(String name){
  this._name = name;
  }

  //location File location
  String get location => this._location;
  set location(String location){
  this._location = location;
  }
  //image File Image
  Warp get image => this._image;
  set image(Warp image){
  this._image = image;
  } 
}

List<String> backgroundChars = [
"Chicken",
"Fox",
"Hunters",
"JamesRock",
"Jay",
"Preacher",
"Thug",
];
List<String> backgrounds = [
"Desktop_landscape",
"Desktop_Portrait",
];
